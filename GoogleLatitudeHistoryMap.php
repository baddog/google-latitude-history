<?php 

class GoogleLatitudeHistoryMap {

	function generateMap($attrs, $latitudeHistory, $customStyle, $count) {
			
		$attributes = shortcode_atts( array(
			'height' => 400, // number for pixel
			'width' => 600, // number for pixel
			'max' => 30, // number of locations
			'maptype' => 'TERRAIN', // HYBRID, ROADMAP, SATELLITE, TERRAIN
			'mapcontrol' => 'true', //true/false
			'controlstyle'=> 'DROPDOWN_MENU', // DEFAULT, DROPDOWN_MENU, HORIZONTAL_BAR
			'zoomcontrol' => 'true', //true/false
			'zoomstyle' => 'SMALL', // DEFAULT, SMALL, LARGE
			'pancontrol' => 'false', // true/false
			'streetcontrol' => 'false', // true/false
			'haveoverview' => 'false', // true/false
			'openoverview' => 'false', // true/false
			'disabledefaultui' => 'false', //true/false
			'showpath' => 'true', // true/false
			'pathcolor' => 'black', // CSS3 colors  #6699ff (blue)
			'pathweight' => 1, // number of pixels
			'pathopacity' => 0.1, //0.0 - 1.0
			'currentcolor' => '#FF0000', // CSS3 colors in HEX
			'pastcolor' => '#6699ff', // CSS3 colors in HEX
			'openwindow' => 'true', //true/false
			'disablewindow' => 'false', //true/false
			'style' => '' //css
		), $attrs);
		
		foreach($attributes as $key => $value) {
			$lcValue = strtolower($value);
			if( $lcValue == 'true' ) {
				$attributes[$key] = true;
			} elseif( $lcValue == 'false' ) {
				$attributes[$key] = false;
			}
		}
		
		if($attributes['style'] != '') {
			$customStyle = $attributes['style']; 
		}
		
		ob_start();

		// html stuff is all on line b/c a user had <p> inserted at the returns causing the javascript to break
		// this fixed the problem, i have no clue why that happened. http://wordpress.org/support/topic/plugin-google-latitude-history-not-displaying-a-map 
	?> <div id="gLatitudeHistoryMap_<?php echo $count; ?>" class="gLatitudeHistoryMap" style="height:<?php echo $attributes['height']; ?>px; width:<?php echo $attributes['width']; ?>px;<?php echo $customStyle; ?>"></div> <script type="text/javascript"><?php if($count == 1):?> var gLatitudeHistoryLocations = new Array(); var gLatitudeHistoryAttributes = new Array(); <?php endif;?> gLatitudeHistoryLocations[<?php echo $count; ?>] = <?php echo json_encode($latitudeHistory); ?>; gLatitudeHistoryAttributes[<?php echo $count; ?>] = <?php echo json_encode($attributes); ?>; </script> <?php 
	
		return ob_get_clean();
	}
} 

?>