<?php
/*
Plugin Name: Google Latitude History
Plugin URI: http://worldtravelblog.com/code/google-latitude-history-plugin
Description: This plugin allows you to display your Google Latitude location history on Google Maps
Version: 0.9.9
Author: Peter Rosanelli
Author URI: http://www.worldtravelblog.com
*/

/**
* LICENSE
* This file is part of Google Latitude History Plugin.
*
* Google Latitude History Plugin is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
* @package    google-latitude-history
* @author     Peter Rosanelli <peter@worldtravelblog.com>
* @copyright  Copyright 2011 Peter Rosanelli
* @license    http://www.gnu.org/licenses/gpl.txt GPL 2.0
* @version    0.9.9
* @link       http://worldtravelblog.com/code/google-latitude-history-plugin
*/


@session_start();
require_once 'google-api-php-client/src/apiClient.php';
require_once 'google-api-php-client/src/contrib/apiLatitudeService.php';

global $wpdb;

// settings page
require_once('glatitudehistory_settings.php');
// generates map from shortcode
require_once('GoogleLatitudeHistoryMap.php');
// database access object
require_once('GoogleLatitudeHistoryDAO.php');

if(class_exists('GoogleLatitudeHistory')) {
	$plugin_glatitudehistory = new GoogleLatitudeHistory();
}

if(isset($plugin_glatitudehistory)) {
	/**
	 * Creates plugin option page
	 * This is outside of the class b/c syncLocations()
	 * which is called from options page will fail. it fails
	 * b/c $this is called outside of the object context. 
	 */
	function glatitudehistory_admin_menu() {
		global $plugin_glatitudehistory;
		add_options_page('Google Latitude History Settings', 'Google Latitude History', 'manage_options', 'google-latitude-history-menu', array(&$plugin_glatitudehistory, 'adminMenuOptions' ) );
	}
	
	register_activation_hook( __FILE__,  array(&$plugin_glatitudehistory, 'install' ));
	register_activation_hook(__FILE__, array(&$plugin_glatitudehistory, 'scheduleCron' ));
	register_deactivation_hook(__FILE__, array(&$plugin_glatitudehistory, 'unscheduleCron' ));
	register_deactivation_hook(__FILE__, array(&$plugin_glatitudehistory, 'dropTables' ));

	// actions
	add_action('wp_enqueue_scripts', array(&$plugin_glatitudehistory, 'wpEnqueueScripts'));
	add_action('admin_enqueue_scripts', array(&$plugin_glatitudehistory, 'adminEnqueueScripts'));
	add_action('admin_menu', 'glatitudehistory_admin_menu');
	add_action('admin_print_scripts-settings_page_google-latitude-history-menu', array(&$plugin_glatitudehistory, 'adminPrintScripts'));
	add_action('admin_print_styles-settings_page_google-latitude-history-menu', array(&$plugin_glatitudehistory, 'adminPrintStyles'));	
	add_action('admin_notices', array(&$plugin_glatitudehistory, 'checkToken'));	
	add_action('wp_head', array(&$plugin_glatitudehistory, 'wpHead'));
	add_action('wp_footer', array(&$plugin_glatitudehistory, 'wpFooter'));
	add_action('hourly_latitude_sync', array(&$plugin_glatitudehistory, 'syncLocations'));
	add_action('wp_ajax_delete_location', array(&$plugin_glatitudehistory, 'delete_location_callback'));
	add_action('wp_ajax_update_location', array(&$plugin_glatitudehistory, 'update_location_callback'));
	add_shortcode('glatitude-map', array(&$plugin_glatitudehistory, 'shortcodeHandler'));
}

class GoogleLatitudeHistory {
	
	const DEFAULT_MAX_LOCATIONS = 20;
	const MAX_LOCATIONS = 100;
	const DEFAULT_CSS = 'margin:0 auto;font-size:12px;line-height:16px;';
	const RESPONSE_FIELDS = 'items(latitude,longitude,timestampMs,accuracy)'; // restrict response to relevant fields
	
	// determines if shortcode has been used on a page, if so then print the associated javascript
	static $add_script;
	
	// for counting the number of map instances on a single page and gives the data different ids
	static $instanceCount = 0;
	
	public $oauthClient;
	public $oauthService;
	public $redirectUrl;
	public $dao;
	
	function GoogleLatitudeHistory() {
		
		$this->dao = new GoogleLatitudeHistoryDAO();
		
		$this->redirectUrl = get_admin_url() . 'options-general.php?page=google-latitude-history-menu';

		$client = new apiClient();
		$client->setClientId(get_option('glatitudehistory_client_id'));
		$client->setClientSecret(get_option('glatitudehistory_client_secret'));
		$client->setRedirectUri($this->redirectUrl);
		$client->setApplicationName(get_bloginfo('name'));
		if(get_option('glatitudehistory_access_token') != null) {
			$client->setAccessToken(get_option('glatitudehistory_access_token'));
		}
		$this->oauthClient = $client;
		$service = new apiLatitudeService($client);
		$this->oauthService = $service;
		
		if (isset($_GET['code'])) {
			try {
				
				$client->authenticate();
  				update_option('glatitudehistory_access_token', $client->getAccessToken());
  				update_option('glatitudehistory_fresh_token', true);
			} catch (Exception $e) {
				// how to let the user know?
				update_option('glatitudehistory_access_token', null);
				update_option('glatitudehistory_fresh_token', false);
			}
			
			header('Location: ' . $_SERVER['SCRIPT_NAME'] . '?page=google-latitude-history-menu');
		}
	}
	
	function checkToken() {	
		if(!get_option('glatitudehistory_fresh_token')) {
			echo '<div class="error"><p><span style="font-weight:bold">Google Latitude History: </span>Your OAuth token is stale, please refresh the token on the <a href="options-general.php?page=google-latitude-history-menu">settings page</a>. If you are having trouble, please double check the client ID and secret are correct.</p></div>';
		}		
	}
	
	/**
	* Calls the Google Latitude rest service to get the location history and 
	* updates the database.
	* 
	* @return object latitude response
	*/
	function syncLocations() {
		
		$options = array();
		
		$options['max-results'] = get_option('glatitudehistory_max_locations');
		
		// min/max-time is milliseconds since the epoch hence multipling by 1000
		if(get_option('glatitudehistory_min_time')) {
			$options['min-time'] = strtotime(get_option('glatitudehistory_min_time'))*1000;
		}
		if(get_option('glatitudehistory_max_time')) {
			$options['max-time'] = strtotime(get_option('glatitudehistory_max_time'))*1000;
		}
		
		$options['granularity'] = get_option('glatitudehistory_granularity');
		
		// restrict response to relevant fields
		$options['fields'] = self::RESPONSE_FIELDS;

		try {
			// get locations from glatitude
			$latitudeResponse = $this->oauthService->location->listLocation($options);
			// update database
			$this->updateDatabase($latitudeResponse);
			
			update_option('glatitudehistory_fresh_token', true);
		} catch (Exception $e) {	
			update_option('glatitudehistory_fresh_token', false);
			throw new Exception($e->getMessage());
		}

		return $latitudeResponse;
	}
	
	/**
	* Takes the location history and compares it to whats in the 
	* the database then adds new locations and removes old ones
	* from the database
	*
	* @param object location history
	*/
	function updateDatabase($latitudeHistory) {
		global $wpdb;
	
		// create hash of location info by timestamp from glatitude
		// and put the glatitude locations (new locations) in an array for comparison to the database locaations
		$googleTimestamps = array();
		$googleLocationsByTimestamp = array();
		if($latitudeHistory->getItems()) {
			foreach($latitudeHistory->getItems() as $location) {
				if($location->getTimestampMs()) {
					$googleTimestamps[] = $location->getTimestampMs();
					$googleLocationsByTimestamp[$location->getTimestampMs()] = $location; 
				} else {
					throw new Exception('Empty Google Latitude location data. <a href="http://code.google.com/p/latitude-api/issues/detail?id=30" target="_blank">bug report</a>');
				}
			}
		} else {
			throw new Exception('No Google Latitude locations found.');
		}
			
		// put the database locataions ("old locations") into an array for comparison to the glatitude locations
		$wpHistory = $this->dao->selectAll();
		$wpTimestamps = array();
		foreach($wpHistory as $location) {
			$wpTimestamps[] = $location->timestamp_ms;
		}
		
		// find new locations from glatitude
		$newTimestamps = array_diff($googleTimestamps, $wpTimestamps);
	
		// find old locations in the database
		$oldTimestamps = array_diff($wpTimestamps, $googleTimestamps);
			
		// insert new locations into database
		foreach($newTimestamps as $timestamp) {
			$location = $googleLocationsByTimestamp[$timestamp];
			
			$this->dao->insert($location);
		}
			
		// remove old locations from database
		if(count($oldTimestamps) > 0) {
			//$this->deleteLocations($oldTimestamps);
			$this->dao->delete($oldTimestamps);
		}
	}
	
	function update_location_callback() {
		global $wpdb;

		$orginalTime = $_POST['locationId'];
		$newTime = strtotime($_POST['date']) * 1000; // get milliseconds
		
		if($orginalTime != $newTime && $orginalTime != 0) {
			
			try {
				$this->oauthService->location->delete($orginalTime);
				
				$this->dao->delete(array($orginalTime));
			} catch (Exception $e) {
				echo $e->getMessage();	
			}
			
		}

		$data = array(
			'data' => array(
				'kind' => 'latitude#location',
				'timestampMs' => $newTime,
				'latitude' => $_POST['latitude'],
				'longitude' => $_POST['longitude']
			)			
		);
		
		$location = new Location($data);
		
		try {
			$response = $this->oauthService->location->insert($location);
						
			if($orginalTime != $newTime && $orginalTime != 0) {
				$this->dao->insert($response);
			} else {
				$this->dao->update($response);
			}
		} catch (Exception $e) {
			echo $e->getMessage();	
		}
		
		$this->syncLocations();
		
		die(); // this is required to return a proper result
	}
	
	function delete_location_callback() {
		global $wpdb;

		$locationId = $_POST['locationId'];
		
		$this->oauthService->location->delete($locationId);
		
		$this->dao->delete(array($locationId));
		
		$this->syncLocations();
		
		die(); // this is required to return a proper result
	}
		
	/**
	* The function turns the shortcode into html and javascript for displaying the
	* Google Latitude location history on a Google Map
	*
	* @param array shortcode attributes
	* @return string html and javascript for map
	*/
	function shortcodeHandler($attrs) {
		//global $wpdb;
		
		self::$add_script = true;
		
		self::$instanceCount++;
		
		$latitudeHistory = $this->dao->selectShortcode($attrs);
	
		return GoogleLatitudeHistoryMap::generateMap($attrs, $latitudeHistory, get_option('glatitudehistory_custom_css'), self::$instanceCount);
	}
	
	function wpHead() {
		//wp_enqueue_style('google-latitude-history2');
	}
	
	/**
	* Decides whether the javascript files with be included in a page
	*/
	function wpFooter() {
		if ( ! self::$add_script )
		return;
		
		// 
		if(get_option('glatitudehistory_google_api_ssl') == 'true' || get_option('glatitudehistory_use_google_api_key') == 'true') {

			wp_deregister_script('google_maps');
			
			$protocol = 'http';
			if(get_option('glatitudehistory_google_api_ssl') == 'true') {
				$protocol = 'https';								
			}
			
			$urlKey = '';
			if(get_option('glatitudehistory_use_google_api_key') == 'true') {
				$urlKey = '&key='.get_option('glatitudehistory_google_api_key');
			}
			
			$googleMapApiUrl = $protocol.'://maps.googleapis.com/maps/api/js?sensor=false'.$urlKey;
			
			wp_register_script('google_maps', $googleMapApiUrl, false, '3', true);
		}
		
		wp_enqueue_style('google-latitude-history2');
		
		// used print instead of enqueue b/c it doesnt work when printing the js on the bases of a variable 
		wp_print_scripts('google_maps');
		wp_print_scripts('styled-marker');
		wp_print_scripts('google-latitude-history');
	}
	
	/**
	*  add google latitude history link to the admin menu
	*/
	function adminMenu() {
		add_options_page('Google Latitude History Settings', 'Google Latitude History', 'manage_options', 'google-latitude-history-menu', array('GoogleLatitudeHistory', 'adminMenuOptions' ) );
	}
	
	/**
	* This function handles the plugin admin settings page which includes
	* updating the settings, syncing the database with Google Latitude, and
	* displaying the settings page 
	*
	* @return string the settings page as html
	*/
	function adminMenuOptions() {
		global $wpdb;
		
		echo '<h2>Google Latitude History Settings</h2>';
		
			wp_nonce_field( 'update-options' );

			if(isset($_POST['submit']) && $_POST['submit'] == 'Update Options') {

				// Update Google Latitude Settings
				update_option('glatitudehistory_google_api_key', $_POST['google_api_key']);				
				
				if(isset($_POST['google_api_ssl']) && $_POST['google_api_ssl'] == 'true') {
					update_option('glatitudehistory_google_api_ssl', $_POST['google_api_ssl']);	
				} else {
					update_option('glatitudehistory_google_api_ssl', '');
				}
				
				if(isset($_POST['use_google_api_key']) && $_POST['use_google_api_key'] == 'true') {
					update_option('glatitudehistory_use_google_api_key', $_POST['use_google_api_key']);	
				} else {
					update_option('glatitudehistory_use_google_api_key', '');
				}
				
				update_option('glatitudehistory_client_id', $_POST['client_id']);
				update_option('glatitudehistory_client_secret', $_POST['client_secret']);
				update_option('glatitudehistory_granularity', $_POST['granularity']);
				update_option('glatitudehistory_custom_css', $_POST['custom_css']);
				
				// check if max locations parameter is a number and gt 0 and lt MAX_LOCATIONS
				// if not then set it to the default number of locations
				update_option('glatitudehistory_max_locations', self::DEFAULT_MAX_LOCATIONS);
				if( is_numeric($_POST['max_locations']) ) {
					$max_locations = (int) $_POST['max_locations'];
					if($max_locations > 0 && $max_locations < self::MAX_LOCATIONS) {
						update_option( 'glatitudehistory_max_locations', $max_locations);
					}
				}
				
				update_option('glatitudehistory_min_time', $_POST['min_time']);
				update_option('glatitudehistory_max_time', $_POST['max_time']);
				
				echo '<div class="updated"><p>Settings Updated</p></div>';
			
			} elseif(isset($_POST['submit']) && $_POST['submit'] == 'Sync Location History' ) {
				
				$statusMessage = '';
				
				try { 
					$latitudeResponse = $this->syncLocations();
					
					$statusMessage = '<div class="updated"><p>Google Latitude History was synced <br/>';
					$statusMessage .= '<a href="#" id="toggle_response">see latitude response</a>';
					$statusMessage .= '<div id="json_response" style="display:none">'.json_encode($latitudeResponse).'</div>';
					$statusMessage .= '</p></div>';					
				} catch (Exception $e) {
					$statusMessage .= '<div class="error"><p>Error syncing Latitude location history: '.$e->getMessage();
					$statusMessage .= '</p></div>';
				}
				
				// sync status message
				echo $statusMessage;
			}
			
			$this->oauthClient->setClientId(get_option('glatitudehistory_client_id'));
			$this->oauthClient->setClientSecret(get_option('glatitudehistory_client_secret'));
			
			// last location is the first result
			$latitudeHistory = $this->dao->selectAll();
					
			// display the settings page
			glatitudehistory_settings($this->oauthClient->createAuthUrl(), $this->redirectUrl, $latitudeHistory, self::DEFAULT_CSS);
	}
	
	/**
	 * Scripts printed on the Google Latitude History admin page
	 */
	function adminPrintScripts() {
		wp_enqueue_script('jquery-ui-accordion');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('jquery-ui-dialog');
		wp_enqueue_script('datetimepicker');
		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_script('timepicker');
		wp_enqueue_script('google_maps');
		wp_enqueue_script('styled-marker');
		wp_enqueue_script('jquery-blockUI');
		wp_enqueue_script('admin');
	}
	
	/**
	 * Styles printed on the Google Latitude History admin page
	 */
	function adminPrintStyles() {
		wp_enqueue_style('admin');
		wp_enqueue_style('jquery-ui-redmond');
	}
	
	
	/**
	* Install Google Latitude History Plugin.
	* Register settings, set defaults, and create database table
	*/
	function install() {
		
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_google_api_key' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_google_api_ssl' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_use_google_api_key' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_client_id' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_client_secret' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_access_token' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_granularity' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_max_locations' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_location' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_custom_css' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_min_time' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_max_time' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_latitude_api_key' );
		register_setting( 'glatitudehistory-settings-group', 'glatitudehistory_fresh_token' );
		
		// default latitude query parameter for location history
		update_option('glatitudehistory_location', 'all'); 
		// default maximum number of locations
		update_option('glatitudehistory_max_locations', self::DEFAULT_MAX_LOCATIONS);
		// default css for map div
		update_option('glatitudehistory_custom_css', self::DEFAULT_CSS);
				
		$this->createTables();
	}
	
	/**
	* Creates the database tables used to store Google Latitude locations
	*/
	function createTables() {
		$this->dao->createLatitudeTable();
   		
   		$this->dao->createApiCacheTable();
	}
	
	/**
	* Drops the table used to store Google Latitude locations and api cache
	*/
	function dropTables() {
		$this->dao->dropLatitudeTable();
		
		$this->dao->dropApiCacheTable();
	}
	
	
	/**
	* Creates sync location cron job
	*/
	function scheduleCron() {
		wp_schedule_event(time(), 'hourly', 'hourly_latitude_sync' );
	}
	
	/**
	* Removes sync location cron job
	*/
	function unscheduleCron() {
		wp_clear_scheduled_hook( 'hourly_latitude_sync' );
	}
	
	function wpEnqueueScripts() {
		// shortcode scripts
		wp_register_script('google_maps', 'http://maps.googleapis.com/maps/api/js?sensor=false', false, '3', true);
		wp_register_script('styled-marker', plugins_url('js/StyledMarker.js', __FILE__), array('google_maps'), '0.5', true);
		wp_register_script('google-latitude-history', plugins_url('js/glatitudehistory.js', __FILE__), array('jquery', 'google_maps', 'styled-marker'), '0.9.9', true);

		// shortcode styles
		wp_register_style('google-latitude-history2', plugins_url('css/glatitudehistory.css', __FILE__), array(), '0.9.9', 'screen');
	}
	
	function adminEnqueueScripts() {
		
		wp_register_script('google_maps', 'http://maps.googleapis.com/maps/api/js?sensor=false', false, '3', true);
		wp_register_script('styled-marker', plugins_url('js/StyledMarker.js', __FILE__), array('google_maps'), '0.5', true);
		wp_register_script('google-latitude-history', plugins_url('js/glatitudehistory.js', __FILE__), array('jquery', 'google_maps', 'styled-marker'), '0.9.9', true);
		
		// admin scripts
		wp_register_script('jquery-blockUI', plugins_url('js/jquery.blockUI.js', __FILE__), array('jquery'), '2.39', true);
		wp_register_script('jquery-ui-datepicker', plugins_url('js/jquery.ui.datepicker.min.js', __FILE__), array('jquery-ui-core'), '1.8.16', true);
		wp_register_script('datetimepicker', plugins_url('js/datetimepicker.js', __FILE__), array('jquery-ui-datepicker'), '0.9.9', true);
		wp_register_script('jquery-ui-slider', plugins_url('js/jquery.ui.slider.min.js', __FILE__), array('jquery-ui-core', 'jquery-ui-mouse', 'jquery-ui-widget'), '1.8.16', true);
		wp_register_script('timepicker', plugins_url('js/timepicker.js', __FILE__), array('jquery-ui-datepicker', 'jquery-ui-slider'), '0.9.9', true);
		wp_register_script('admin', plugins_url('js/admin.js', __FILE__), array('jquery', 'jquery-blockUI', 'jquery-ui-accordion'), '0.9.9', true);

		// admin styles
		wp_register_style('admin', plugins_url('css/admin.css', __FILE__), array(), '0.9.9', 'screen');
		wp_register_style('jquery-ui-redmond', plugins_url('css/jquery-ui-1.8.18.redmond.css', __FILE__), array(), '1.8.18', 'screen');			
	}
	
}	

?>