<?php function glatitudehistory_settings($authUrl, $redirectUrl, $latitudeHistory, $defaultCSS) { ?>

	<script type="text/javascript">
		var data = <?php echo json_encode($latitudeHistory); ?>;
	</script>

	<input type="hidden" id="defaultCSS" value="<?php echo $defaultCSS; ?>" />

	<form name="latitude_settings" id="latitudeSettings" method="post">
		<br/>
		<p>
			<input type="submit" name="submit" value="Sync Location History" />
		</p>
		
		<br/>

		<div id="page-map"></div>

		<br style="clear:both" />
		<br style="clear:both" />
		<div>
			<a href="#" class="edit" id="edit-0" onclick="return false;">
				<span class="ui-icon ui-icon-plusthick" style="float:left;"></span>
			</a> 
			<a href="#" class="edit" id="edit-0" onclick="return false;">add a new location</a>
		</div>
		<br/>
		<span class="bold">Locations</span>
		<div class="locationTable-container">
			<table class="locationTable-table">
				<thead>
					<tr>
						<th>Timestamp</th>
						<th>Location name</th>
						<th>Coordinates</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($latitudeHistory as $location): ?>
					<tr>	
						<td>
							<?php echo $location->date; ?>
						</td>
						<td>
							<?php echo $location->location_name; ?>
						</td>
						<td>
							<?php echo (float) $location->latitude; ?>, <?php echo (float) $location->longitude; ?>
						</td>
						<td>
							<a href="#" class="edit" id="edit-<?php echo $location->timestamp_ms; ?>" onclick="return false;">
								edit
							</a>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>		
		
		<div id="dialog" title="Edit Location">
		
			<div id="dialog-map" style="height:400px;width:575px;"></div>
			<br/>
			<div style="display:inline;">
				<input type="hidden" id="dialog-timestamp_ms" />
				<label for="dialog-latitude">Latitude: </label><input type="text" id="dialog-latitude" class="coords" size="10" />
				<label for="dialog-longitude">Longitude: </label><input type="text" id="dialog-longitude" class="coords" size="10" />
				<label for="dialog-date">Time: </label><input type="text" id="dialog-datetime" size="25" /> UTC
			</div>				
			
		</div>
	
		<br style="clear:both" />
		<br/>
		<input type="submit" name="submit" value="Update Options" />
		<br/><br/>
		
		<span style="color:red;">*</span> required settings
		
		<br/><br/>
		
		<fieldset>
			<legend>Google Latitude Connection Options</legend>
			<div class="divRow">
				<div class="firstCell">
					<span class="required">*</span>&nbsp;<label for="client_id">Client ID:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="client_id" id="client_id" class="regular-text" value="<?php echo get_option('glatitudehistory_client_id'); ?>" />
				</div>
			</div>	

			<div class="divRow">
				<div class="firstCell">
					<span class="required">*</span>&nbsp;<label for="client_secret">Client Secret:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="client_secret" id="client_secret" class="regular-text" value="<?php echo get_option('glatitudehistory_client_secret'); ?>" />	
				</div>
			</div>

			<div class="divRow">
				<div class="firstCell">
					<label for="redirect_uri">&nbsp;&nbsp;&nbsp;Redirect URI:</label>
				</div>
				<div class="secondCell">
					<textarea id="redirect_uri" class="regular-text" readonly="readonly" style="width:100%;height:60px;" ><?php echo $redirectUrl; ?></textarea>
				</div>
			</div>
			
			<div class="divRow">
				<div class="firstCell">
					&nbsp;<!-- empty -->
				</div>
				<div class="secondCell">
					<a href="https://code.google.com/apis/console/" target="_blank" style="float:right;">Google API Console</a>
				</div>
			</div>
			
			<div class="divRow">
				<div class="firstCell">
					<span class="required">*</span>&nbsp;<label for="granularity">Granularity:</label>		
				</div>
				<div class="secondCell">
					<select name="granularity" id="granularity" style="width:6em;">
						<option value="best" <?php if(get_option('glatitudehistory_granularity') == 'best') { echo 'selected="selected"'; } ?> >best</option>
						<option value="city" <?php if(get_option('glatitudehistory_granularity') == 'city') { echo 'selected="selected"'; } ?> >city</option>
					</select>
				</div>
			</div>	
			
			<div class="divRow">
				<a href="<?php echo $authUrl ?>" id="refreshToken">Refresh OAuth Token</a>
			</div>
			
		</fieldset>
		
		<fieldset>
			<legend>Database Storage Options</legend>
		
			<div class="divRow">
				<div class="firstCell">
					<label for="min_time">&nbsp;&nbsp;&nbsp;Minimum Time:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="min_time" id="min_time" class="datepicker regular-text" value="<?php echo get_option('glatitudehistory_min_time'); ?>" style="width:120px;" /> UTC	
				</div>
			</div>
		
			<div class="divRow">
				<div class="firstCell">
					<label for="max_time">&nbsp;&nbsp;&nbsp;Maximum Time:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="max_time" id="max_time" class="datepicker regular-text" value="<?php echo get_option('glatitudehistory_max_time'); ?>" style="width:120px;" /> UTC 		
				</div>
			</div>
		
			<div class="divRow">
				<div class="firstCell">
					<span class="required">*</span>&nbsp;<label for="max_locations">Maximum Number of Points:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="max_locations" id="max_locations" class="regular-text" value="<?php echo get_option('glatitudehistory_max_locations'); ?>" maxlength="2" style="width:4em;" />
				</div>
			</div>
		</fieldset>
		
		<fieldset>
			<legend>Google Map Options</legend>
		
			<div class="divRow">
				<div class="firstCell">
					<label for="google_api_key">&nbsp;&nbsp;&nbsp;Google API Key:</label>
				</div>
				<div class="secondCell">
					<input type="text" name="google_api_key" id="google_api_key" class="regular-text" value="<?php echo get_option('glatitudehistory_google_api_key'); ?>" <?php if(get_option('glatitudehistory_use_google_api_key') != 'true') { echo 'readonly="readonly"'; } ?>/>		
				</div>
			</div>
			
			<div class="divRow">
				<div class="firstCell">
					<label for="use_google_api_key">&nbsp;&nbsp;&nbsp;Use Google API Key:</label>
				</div>
				<div class="secondCell">
					<div style="float:left;"><input type="checkbox" name="use_google_api_key" id="use_google_api_key" value="true" <?php if(get_option('glatitudehistory_use_google_api_key') == 'true') { echo 'checked="checked"'; } ?> /></div>
					<div style="float:right;"><a href="https://code.google.com/apis/console" target="_blank">Google API Console</a></div>				
				</div>
			</div>
			
			<div class="divRow">
				<div class="firstCell">
					<label for="google_api_ssl">&nbsp;&nbsp;&nbsp;Google Maps over SSL:</label>
				</div>
				<div class="secondCell">
					<input type="checkbox" name="google_api_ssl" id="google_api_ssl" value="true" <?php if(get_option('glatitudehistory_google_api_ssl') == 'true') { echo 'checked="checked"'; } ?> />
				</div>
			</div>

			<div class="divRow">
				<div class="firstCell">
					<label for="custom_css">&nbsp;&nbsp;&nbsp;Custom CSS for Map Div:</label>
				</div>
				<div class="secondCell">
					<textarea name="custom_css" id="custom_css" class="regular-text" style="width:300px;height:70px;"><?php echo get_option('glatitudehistory_custom_css'); ?></textarea>
					<br/>
					<a href="#" id="replace_css" onclick="return false;">replace with default css</a>
				</div>
			</div>
		</fieldset>	

			
		<p>
			<input type="submit" name="submit" value="Update Options" />
		</p>
		<br/>
		<p class="bold">ShortCode Options for [glatitude-map /]</p>
		<ul style="list-style-type:circle;padding-left:20px;">
			<li>height = height of the map in pixels</li>
			<li>width = width of the map in pixels</li>
			<li>max = maximum number of locations to display on the map</li>
			<li>maptype = Google map type ( HYBRID, ROADMAP, SATELLITE, TERRAIN )</li>
			<li>mapcontrol = show map type control ( true / false )</li>
			<li>controlstyle = map control style ( DEFAULT, DROPDOWN_MENU, HORIZONTAL_BAR )</li>
			<li>zoomcontrol = show zoom controls ( true / false )</li>
			<li>zoomstyle = zoom control style ( DEFAULT, SMALL, LARGE )</li>
			<li>pancontrol = show pan control ( true / false )</li>
			<li>streetcontrol = show street view control ( true / false )</li>
			<li>haveoverview = include overview map button ( true / false )</li>
			<li>openoverview = open overview map ( true / false )</li>
			<li>disabledefaultui = disable default UI ( true / false )</li>
			<li>showpath = show path between the tweets ( true / false )</li>
			<li>pathcolor = color of the path ( HTML color codes )</li>
			<li>pathweight = weight of the path in pixels</li>
			<li>pathopacity = opacity of the path ( 0.0 - 1.0 )</li>
			<li>currentcolor = color of the marker for the latest location ( HTML color codes )</li>
			<li>pastcolor = color of the marker for past locations ( HTML color codes )</li>
			<li>openwindow = open current location's info window on load ( true / false )</li>
			<li>disablewindow = disable the markers' info window ( true / false )</li>
			<li>mintime = location timestamps must be greater then or equal to this time in mm/dd/YYYY or mm/dd/YYYY hh:ss format</li>
			<li>maxtime = location timestamps must be less then or equal to this time in mm/dd/YYYY or mm/dd/YYYY hh:ss format</li>
			<li>style = add css style to the map div, it will override the custom css in the plugin settings</li>
		</ul>
	</form>
	
	<br/>
	
	<a href="http://wordpress.org/extend/plugins/google-latitude-history/installation/" target="_blank">Installation instructions</a>

<?php } ?>