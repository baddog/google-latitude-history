=== Google Latitude History ===
Contributors: peter2322
Donate link: http://worldtravelblog.com/code/google-latitude-history-plugin/
Tags: google, latitude, maps, location
Requires at least: 3.1.3
Tested up to: 3.3.1
Stable tag: 0.9.9

This plugin will display your Google Latitude location history on a Google Map and allow you to manage your Latitude locations.

== Description ==

The Google Latitude History plugin provides shortcode to display and customize your Latitude location history on a map. The plugin runs on a hourly wordpress cron to keep your locations up to date. Just use [glatitude-map /] on any post or page to see a map of your location history. This plugin also can create, update, and delete your Latitude locations. 

== Installation ==

1. Upload the folder containing the plugin files.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Sign up for Google Latitude at [http://www.google.com/latitude](http://www.google.com/latitude "Google Latitude").
4. Register for Google API service at [https://code.google.com/apis/console](https://code.google.com/apis/console "Google API Console").
5. In the [Google API Console](https://code.google.com/apis/console "Google API Console") click on 'API Access' and update your Google Latitude History plugin settings with your Client ID and Client Secret (screenshot 3).
6. Copy the Redirect URI in the Google Latitude History settings page, click 'edit settings' in  the under API Access in the [Google API Console](https://code.google.com/apis/console "Google API Console") and paste the URI into the 'Authorized Redirect URIs' box (screenshot 6).  
7. \[Optional\] In the [Google API Console](https://code.google.com/apis/console "Google API Console") click on 'Services' and turn on 'Google Maps API v3'. Then click on 'API Access' and update your Google Latitude History plugin settings with your Simple API key. The api key tracks usuage and is not necessary. If your website uses a SSL certificate then set Google Maps to use SSL (screenshot 4).
8. Refresh OAuth token and grant access to this plugin.
9. Click sync location history.
10. Add [glatitude-map /] to any page to see your map.
11. Your location history will update automatically every hour.

== Shortcode Options ==

* height = height of the map in pixels
* width = width of the map in pixels
* max = maximum number of locations to display on the map
* maptype = Google map type ( HYBRID, ROADMAP, SATELLITE, TERRAIN )
* mapcontrol = show map type control ( true / false )
* controlstyle = map control style ( DEFAULT, DROPDOWN_MENU, HORIZONTAL_BAR )
* zoomcontrol = show zoom controls ( true / false )
* zoomstyle = zoom control style ( DEFAULT, SMALL, LARGE )
* pancontrol = show pan control ( true / false )
* streetcontrol = show street view control ( true / false )
* haveoverview = include overview map button ( true / false )
* openoverview = open overview map ( true / false )
* disabledefaultui = disable default UI ( true / false )
* showpath = show path between the locations ( true / false )
* pathcolor = color of the path ( HTML color codes )
* pathweight = weight of the path in pixels
* pathopacity = opacity of the path ( 0.0 - 1.0 )
* currentcolor = color of the marker for the current location ( HTML color codes )
* pastcolor = color of the marker for past locations ( HTML color codes )
* openwindow = open current location's info window on load ( true / false )
* disablewindow = disable the markers' info window ( true / false ) 
* mintime = location timestamps must be greater then or equal to this time in mm/dd/YYYY or mm/dd/YYYY hh:ss format
* maxtime = location timestamps must be less then or equal to this time in mm/dd/YYYY or mm/dd/YYYY hh:ss format
* style = add css style to the map div, it will override the custom css in the plugin settings

== Usage ==

* [glatitude-map /] = standard
* [glatitude-map height="300" width="500" /] = change the map height and width
* [glatitude-map maptype="SATELLITE" /] = show a satellite map
* [glatitude-map currentcolor="#00FF09" pastcolor="#FFFF00" /] = change the current location marker to green and the past location markers to yellow
* [glatitude-map max="80" /] = increase the maximum number of locations to 80
* [glatitude-map mapcontrol="false" /] = remove the map type control 
* [glatitude-map pathcolor="FF0000" pathopacity="0.5" pathweight="3" /] = change the path color, opacity, and the weight
* [glatitude-map mintime="04/20/2011 4:23" /] = only show locations on or after April 20, 2011 at 4:23
* etc etc

== Frequently Asked Questions == 

= Stale Token with 'invalid_client'? = 

Double check your Client Secret with the one in [https://code.google.com/apis/console](https://code.google.com/apis/console "Google API Console").

= Why is my location data empty? =

The Google Latitude product is still in beta therefore bugs will come up from time to time. Google has confirmed a [bug](http://code.google.com/p/latitude-api/issues/detail?id=30 "bug") which results in empty location data. Changing your granularity to "best" might fix the problem. 

= What is wrong with my info windows? = 

The info windows are inheriting CSS styles from the page. Please override the offending styles by using the custom css option on the plugin settings page. If the offending CSS is not overrided check the style.css of your theme and look for the max-width property of images and set it to 'none'.

= Fatal error: Call to undefined function curl_init() =

If you get: "Fatal error: Call to undefined function curl_init(). Go to the php.ini and uncomment "extension=php_curl.dll" and restart your server. 

= Where can I find HTML color codes? =

[http://html-color-codes.info/](http://html-color-codes.info/ "HTML color codes")

= When I attempt to refresh my token I get a "OAuth 2.0 error: redirect_uri_mismatch" error =

Add your redirect uri, located in the Google Latitude History plugin settings page, to the Google API console. See screenshot 6.

= How often are my locations updated? =

The locations are updated hourly by a Wordpress cron

= Where do the location names come from? =

The location names are found by using Google's geocode service to find the location name by using it's latitude and longitude coordinates

= Why doesn't my timestamp match? =

The time is shown in Coordinated Universal Time (UTC)

= Do I need to use the Google Simple API key? =

The Google Simply API key allows you to monitor to your usuage. This is important for highly trafficked websites that might approach the limits of the free service. For more information about usuage limits please visit [http://code.google.com/apis/maps/documentation/javascript/usage.html#usage_limits](http://code.google.com/apis/maps/documentation/javascript/usage.html#usage_limits "Usuage Limits") 

= Do I need to use Google Maps over SSL? = 

If you are using this plugin on a website that uses an SSL certificate, your url will start with "https://", then yes.

== Screenshots ==

1. Example of a Google Latitude history map
2. Second example of a Google Latitude history map
3. Where to find the Client ID, Client secret, and Simple API key at [https://code.google.com/apis/console](https://code.google.com/apis/console "Google API Console") 
4. Where to turn on the Google services at [https://code.google.com/apis/console](https://code.google.com/apis/console "Google API Console").
5. Google Latitude History plugin settings page.
6. How to update the Redirect URI in the [https://code.google.com/apis/console](https://code.google.com/apis/console "Google API Console").

== Changelog ==

= 0.9.9 =

* added support to manage your Latitude locations
* fixed info window css
* fixed drop tables

= 0.9.8 =

* Changed Google API PHP Client caching from file based to use the WordPress database
* Catch of invalid client secret error

= 0.9.7 =

* Upgraded to OAuth 2

= 0.9.6 =

* updated Google Map url
* added option to use Google Simple API key with Google Maps
* added option to use Google maps over SSL
* added shortcode option to add local style to the Google map
* added shortcode option to disable the map markers' infowindows 

= 0.9.5 =

* added support for multiple maps on a single page
* fixed shortcode option (openwindow) for the current location window to be open when the map is loaded.
* fixed shortcode option for the street view control

= 0.9.4 =

* added granularity to rest request

= 0.9.3 =

* Fixed map zoom without points
* Added latitude response to sync status message 

= 0.9.2 = 

* Added minimum and maximum time options for Latitude history retrieval
* Added minimum and maximum time shortcode options for Latitude history map
* fixed php/javascript bug in settings page 

= 0.9.1 =

* checked IE select option bug
* added option to open the current locations's infowindow upon loading
* displayed shortcode options to settings page

= 0.9 = 

* Initial checkin

== Upgrade Notice ==

* Please upgrade if you having trouble with creating the storage directory