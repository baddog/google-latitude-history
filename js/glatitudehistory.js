var gLatitudeHistoryInfowindows = new Array();

jQuery(document).ready(function() {
	jQuery('.gLatitudeHistoryMap').each(function(){
		var index = jQuery(this).attr('id').split('_')[1];
		createGLatitudeHistoryMap(index);
	});
});

function createGLatitudeHistoryMap(index) {
	
	var attrs = gLatitudeHistoryAttributes[index];
	
	var gLatitudeHistoryOptions = {
		zoom: 1,
		center: new google.maps.LatLng(0,0),
		mapTypeId: google.maps.MapTypeId[attrs['maptype']],
		mapTypeControl: attrs['mapcontrol'],
		mapTypeControlOptions: {
		    style: google.maps.MapTypeControlStyle[attrs['controlstyle']]
		},	
		zoomControl: attrs['zoomcontrol'],
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle[attrs['zoomstyle']]
		},
		panControl: attrs['pancontrol'],
		streetViewControl: attrs['streetcontrol'],
		overviewMapControl : attrs['haveoverview'],
		overviewMapControlOptions: {
			opened : attrs['openoverview']
		},
		disableDefaultUI: attrs['disabledefaultui']
	};

	var gLatitudeHistoryMap = new google.maps.Map(document.getElementById("gLatitudeHistoryMap_"+index), gLatitudeHistoryOptions);
	
	var bounds = new google.maps.LatLngBounds();
	var pathCoordinates = new Array();
	var markers = new Array();
	for(var i=0; i < gLatitudeHistoryLocations[index].length; i++) {
		
		var color = attrs['pastcolor'];
		var text = 'Past location';
		if(i == 0) {
			color = attrs['currentcolor'];
			text = 'Current location';
		}	
		
		var zIndex = gLatitudeHistoryLocations[index].length - i;
		var marker = gLatitudeHistoryCreateMarker(gLatitudeHistoryLocations[index][i], text, zIndex, color, gLatitudeHistoryMap, index, attrs['disablewindow']);
		markers.push(marker);
		
		pathCoordinates.push(marker.getPosition());
		bounds.extend(marker.getPosition());
		
		if(i == attrs['max']-1) {
			break;
		}
	}
		
	gLatitudeHistoryCreatePath(gLatitudeHistoryMap, pathCoordinates, attrs);
		
	if(!bounds.isEmpty()) {
		gLatitudeHistoryMap.fitBounds(bounds);
		gLatitudeHistoryMap.setCenter(bounds.getCenter());
	}
	
	// open infowindow after map loads so that the infowindow is in the map
	google.maps.event.addListenerOnce(gLatitudeHistoryMap, 'tilesloaded', function() {
		if(markers.length > 0 && attrs['openwindow']) {
			google.maps.event.trigger(markers[0], 'click');
		}
	});
}

function gLatitudeHistoryCreatePath(map, pathCoordinates, attrs) {
	if( attrs['showpath'] == true) {
		var path = new google.maps.Polyline({
		      path: pathCoordinates,
		      strokeColor: attrs['pathcolor'],
		      strokeOpacity: 1.0,
		      strokeWeight: attrs['pathweight']
		});
		path.setMap(map);
	}
}

function gLatitudeHistoryCreateMarker(location, text, zIndex, color, map, index, disablewindow) {
	
	var latlng = new google.maps.LatLng(location['latitude'], location['longitude']);
	var marker = new StyledMarker({
		styleIcon: new StyledIcon(
			StyledIconTypes.MARKER, { color: color }
		), 
		position: latlng,
		title: location['location_name'],
		flat: true,
		zIndex: zIndex, 
		map: map,
		infowindowContent: '<p style="padding-right:15px;">'+location['location_name']+'<br/>'+text+'<br/>'+location['date']+'</p>'
	});

	if(!disablewindow) {
	
		gLatitudeHistoryInfowindows[index] = new google.maps.InfoWindow({
			content: 'holding...'
		});
		
		google.maps.event.addListener(marker, 'click', function() {
			gLatitudeHistoryInfowindows[index].setContent( marker.infowindowContent );
			gLatitudeHistoryInfowindows[index].open(map, this);
		});
	}
		
	return marker;
}