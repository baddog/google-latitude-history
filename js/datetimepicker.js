jQuery(document).ready(function() {
	
	jQuery('#min_time').datetimepicker({
		maxDate: jQuery('#max_time').val(),
		onSelect: function( selectedDate ) {
			setMinMax(this, selectedDate);
		}
	});
	jQuery('#max_time').datetimepicker({
		minDate: jQuery('#min_time').val(),
		onSelect: function( selectedDate ) {
			setMinMax(this, selectedDate);
		}
	});
	
	function setMinMax(that, selectedDate) {
		var otherId = that.id == "min_time" ? "#max_time" : "#min_time";
		var option = that.id == "min_time" ? "minDate" : "maxDate",
				instance = jQuery(that).data("datepicker"),
				date = jQuery.datepicker.parseDate(
					instance.settings.dateFormat ||
					jQuery.datepicker._defaults.dateFormat,
					selectedDate, instance.settings);
				var otherDate = jQuery(otherId).val();
				jQuery(otherId).datepicker( "option", option, date );
				jQuery(otherId).val(otherDate); // prevent datetime from losing time component	
	}
});