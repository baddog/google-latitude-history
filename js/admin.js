var $j = jQuery.noConflict();

var infowindow = new Array();
var dialogMap;
var dialogMarker;
var defaultLatitude = 40.716667;
var defaultLongitude = -74;

$j(function(){

	$j('#replace_css').click(function() {
		$j('#custom_css').val( $j('#defaultCSS').val() );
		alert('Custom CSS has been replaced with the Default CSS');
	});
	
	toggleResponseClick();
	
	apiKeyClick();
	
	refreshTokenClick();
	
	$j('#dialog-datetime').datetimepicker({
		showSecond: true,
		timeFormat: 'hh:mm:ss tt',
		ampm: true
		/*showMillisec: true,
		timeFormat: 'hh:mm:ss:l'*/
	});
	
	editLocationClick();
	
	createEditLocationDialog();
					
	createPageMap();			
});

function refreshTokenClick() {
	$j('#refreshToken').click(function() {
		if( $j.trim($j('#client_id').val()).length == 0 || $j.trim($j('#client_secret').val()).length == 0) {
			alert('The client id and client secret are necessary to refresh the token. Please update your settings and refresh the token.');
			return false;
		}
	});
}

function toggleResponseClick() {
	$j('#toggle_response').click(function() {
		if( $j('#json_response').css('display') == 'none') {
			$j('#toggle_response').text('hide latitude response');
			$j('#json_response').show();
		} else {
			$j('#toggle_response').text('see latitude response');
			$j('#json_response').hide();
		}
	});
}

function apiKeyClick() {
	$j('#use_google_api_key').click(function() {
		if( $j('#use_google_api_key').attr('checked') ) {
			$j('#google_api_key').removeAttr('readonly');
		} else {
			$j('#google_api_key').attr('readonly', true);
		}
	});
}

function editLocationClick() {
	$j('.edit').live('click', function() {
		
		var id = this.id.split('-')[1];
		
		var location;
		for(var i=0; i < data.length; i++) {
			if(data[i].timestamp_ms == id) {
				location = data[i];
				break;
			}
		}
		if(!location) {
			location = new Array();
			location.timestamp_ms = 0;
			location.latitude = defaultLatitude;
			location.longitude = defaultLongitude;
			
			if(navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position) {
					$j('#dialog-latitude').val( roundCoordinate(position.coords.latitude) );
					$j('#dialog-longitude').val( roundCoordinate(position.coords.longitude) );
			    	var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			    	dialogMarker.setPosition(latLng);
			    	dialogMap.setCenter(latLng)
			    });
			}
		}	
			
		$j('#dialog-timestamp_ms').val(location.timestamp_ms);
		$j('#dialog-date').val(location.date);
		$j('#dialog-latitude').val(parseFloat(location.latitude).toString());
		$j('#dialog-longitude').val(parseFloat(location.longitude).toString());
			
		if(location.timestamp_ms != 0) {
			
			// this song and dance fixes the javascript adjustment for the timezone
			// by adding it back to make it UTC
			var date1 = new Date(0);
			date1.setUTCMilliseconds(location.timestamp_ms);
				
			var diff = date1.getTimezoneOffset() * 60 * 1000; // to get milliseconds
				
			var newTime = parseInt(location.timestamp_ms) + parseInt(diff);
				
			var date2 = new Date(0);
			date2.setUTCMilliseconds(newTime);
				
			$j('#dialog-datetime').datetimepicker('setDate', date2 );
			
		} else {
			// new location so give it current time
			
			// this song and dance fixes the javascript adjustment for the timezone
			// by adding it back to make it UTC			
			var date1 = new Date();
			
			var diff = date1.getTimezoneOffset() * 60 * 1000; // to get milliseconds
		
			var newTime = parseInt(date1.getTime()) + parseInt(diff);
			
			var date2 = new Date(0);
			date2.setUTCMilliseconds(newTime);
			
			$j('#dialog-datetime').datetimepicker('setDate', date2 );
		}
		
		createDialogMap(location);
		
		$j('#dialog').dialog('open');
			
		google.maps.event.trigger(dialogMap, 'resize');
		dialogMap.setCenter(new google.maps.LatLng(location.latitude, location.longitude));
	});
}

function createEditLocationDialog() {
	$j('#dialog').dialog({
		title: 'Edit Location',
		autoOpen: false,
		modal: true,
		minWidth: 600,
		position: 'center',
		buttons: {
			'Delete': function() {
				deleteLocation(this);					
			},
			'Cancel': function() {
				$j(this).dialog('close');
			},
			'Update': function() {
				updateLocation(this);
			}	
		}  
	});
}

function updateLocation(scope) {

	if( isNaN(parseFloat($j('#dialog-latitude').val())) || isNaN(parseFloat($j('#dialog-longitude').val())) ) {
		alert('Fix coordinates');
		return false;
	}
	
	$j(scope).dialog('close');
	
	var data = {
		action: 'update_location',
		locationId: $j('#dialog-timestamp_ms').val(),
		latitude: $j('#dialog-latitude').val(),
		longitude: $j('#dialog-longitude').val(),
		date: $j('#dialog-datetime').val()
	};
		
	$j.blockUI({ 
		message: '<h1><div class="big-ajax-loader" /> Updating location ...</h1>'
	});

	$j.ajax({
		url: ajaxurl,
		type: 'POST',
		data: data,
		timeout: 5000,
		context: this,
		success: function(response) { },
		error: function() {
			alert('There was an error updating the location');
			location.reload();
		},
		complete: function() {
			location.reload();
		}
	});
}
		
		
function deleteLocation(scope) {
	
	$j(scope).dialog('close');
	
	var data = {
		action: 'delete_location',
		locationId: $j('#dialog-timestamp_ms').val()
	};
	
	$j.blockUI({ 
		message: '<h1><div class="big-ajax-loader" /> Deleting location ...</h1>'
	});
	
	$j(this).dialog('close');

	$j.ajax({
		url: ajaxurl,
		type: 'POST',
		data: data,
		timeout: 5000,
		context: this,
		success: function(response) { },
		error: function() {
			alert('There was an error deleting the location');
			location.reload();
		},
		complete: function() {
			location.reload();
		}
	});	
}
		
function createDialogMap(location) {
	var myOptions = {
		center: new google.maps.LatLng(location.latitude, location.longitude),
	    zoom: 12,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControlOptions: {
	    	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},	
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		panControl: false,
		streetViewControl: false
	};

	dialogMap = new google.maps.Map(document.getElementById('dialog-map'), myOptions);
	
	dialogMarker = new google.maps.Marker({
		position: new google.maps.LatLng(location.latitude, location.longitude),
		map: dialogMap,
		title: 'Location',
		draggable: true
	});
	
	$j('.coords').change(function() {
		console.log('change');
		var lat = parseFloat($j('#dialog-latitude').val());
		var lng = parseFloat($j('#dialog-longitude').val());
		if( !isNaN(lat) && !isNaN(lng) ) {
			var newPosition = new google.maps.LatLng( lat, lng );
			dialogMarker.setPosition(newPosition);
			dialogMap.setCenter(newPosition);
		}
	});
	
	google.maps.event.addListener(dialogMarker, 'dragend', function(event) {
		$j('#dialog-latitude').val(roundCoordinate(event.latLng.lat()));
		$j('#dialog-longitude').val(roundCoordinate(event.latLng.lng()));
	});
}
		
function createPageMap() {
	var myOptions = {
		center: new google.maps.LatLng(0, 0),
	    zoom: 1,
	    mapTypeId: google.maps.MapTypeId.TERRAIN,
		mapTypeControlOptions: {
	    	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},	
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		panControl: false,
		streetViewControl: false
	};

	var map = new google.maps.Map(document.getElementById('page-map'), myOptions);

	var bounds = new google.maps.LatLngBounds();
	var pathCoordinates = new Array();
	var markers = new Array();
	for(var i=0; i < data.length; i++) {
		var color = '#6699ff';
		var text = 'Past location';
		if(i == 0) {
			color = '#FF0000';
			text = 'Current location';
		}	
		
		var zIndex = data.length - i;

		var marker = createMarker(data[i], text, zIndex, color, map);
		markers.push(marker);
		
		pathCoordinates.push(marker.getPosition());
		bounds.extend(marker.getPosition());
	}

	if(!bounds.isEmpty()) {
		map.fitBounds(bounds);
		map.setCenter(bounds.getCenter());
	}
}
		
		
function createMarker(location, text, zIndex, color, map) {
	
	var latlng = new google.maps.LatLng(location.latitude, location.longitude);
	var marker = new StyledMarker({
		styleIcon: new StyledIcon(
			StyledIconTypes.MARKER, { color: color }
		), 
		position: latlng,
		title: location.location_name,
		flat: true,
		zIndex: zIndex, 
		map: map,
		infowindowContent: 
			'<p style="padding-right:15px;">'+location.location_name+'<br/>'+text+'<br/>'+location.date+'<br/>'+
			'<a href="#" class="edit" id="editMap-'+location.timestamp_ms+'" onclick="return false;">edit</a></p>'
	});
	
	infowindow = new google.maps.InfoWindow({
		content: 'holding...'
	});
	
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent( marker.infowindowContent );
		infowindow.open(map, this);
	});
		
	return marker;
}

function roundCoordinate(coord) {
	return Math.round(coord*Math.pow(10,7))/Math.pow(10,7);
}