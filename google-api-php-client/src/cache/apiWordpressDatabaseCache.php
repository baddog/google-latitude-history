<?php
/*
 * Copyright 2011 World Travel Blog
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This class implements the cache with a WordPress Database.
 * Below if the WordPress table this class uses. Replace $PREFIX
 * with your WordPress database prefix ($wpdb->prefix) when 
 * creating the table.
 * 
 * CREATE TABLE $PREFIX . _api_cache 
 * (
 * 		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 * 		k VARCHAR(32),  
 * 		data TEXT, 
 * 		last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 * 		INDEX idx_key (k)
 * );
 *
 * @author Peter Rosanelli <peter@worldtravelblog.com>
 */

class apiWordpressDatabaseCache extends apiCache {
	private $apiCacheTable; 

 	public function __construct() {
    	global $apiConfig;
    	global $wpdb;
    	
    	$this->apiCacheTable = $wpdb->prefix . 'api_cache';;
  	}

	/**
	 * Retrieves the data for the given key, or false if they
	 * key is unknown or expired
     *
     * @param String $key The key who's data to retrieve
     * @param boolean|int $expiration Expiration time in seconds
     *
    */
	public function get($key, $expiration = false) {
		global $wpdb;

		$result = $wpdb->get_row("SELECT * FROM " . $this->apiCacheTable . " WHERE K = '" . $key . "'");
		
		if($result) {
			$now = time();
			
      		if (! $expiration || (($mtime = strtotime($result->last_updated)) !== false && ($now - $mtime) < $expiration)) {
      			$data = unserialize($result->data);
          		return $data;
			}
		}
		
		return false;
  	}

	/**
   	 * Store the key => $value set. The $value is serialized
	 * by this function so can be of any type
	 *
	 * @param String $key Key of the data
	 * @param $value the data
	*/
  	public function set($key, $value) {
  		global $wpdb;
  		
  		$data = serialize($value);
  		
  		$result = $wpdb->get_row("SELECT * FROM " . $this->apiCacheTable . " WHERE K = '" . $key . "'");
  		
  		if($result) {
  			
  			 $wpdb->update( $this->apiCacheTable, array(
  			 	'data' => $data
  			 ), array(
  			 	'k' => $key
  			 ), array(
  			 	'%s'
  			 ), array( 
  			 	'%s'
  			 )); 
  			
  		} else {
  			
  			$wpdb->insert( $this->apiCacheTable, array(
				'k' => $key,
				'data' => $data
			), array( '%s', '%s' ) );			
  		}
	}

	/**
	 * Removes the key/data pair for the given $key
	 *
	 * @param String $key
	*/
	public function delete($key) {
  		global $wpdb;
		
		$wpdb->query("DELETE FROM " . $this->apiCacheTable . " WHERE k = '" . $key . "'" );
  
	}
  
  
}