<?php

class GoogleLatitudeHistoryDAO {
	
	const LATITUDE_TABLE = 'glatitude_history'; 
	const CACHE_TABLE = 'api_cache';
	
	function selectAll() {
		global $wpdb;
		
		$rows = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix.self::LATITUDE_TABLE . ' ORDER BY timestamp_ms DESC');
		
		return $rows;
	}
	
	function selectShortcode($attrs) {
		global $wpdb;		
		
		// add min/max time condition
		$condition = ' WHERE ';
		$timeConditionSet = false;
		if(isset($attrs['mintime']) && $attrs['mintime'] != '') {
			$minTime = strtotime($attrs['mintime'])*1000;
			if($minTime) {
				$condition .= ' timestamp_ms >= '.$minTime;
				$timeConditionSet = true;
			}
		}
		if(isset($attrs['maxtime']) && $attrs['maxtime'] != '') {
			$maxTime = strtotime($attrs['maxtime'])*1000;
			if($maxTime) {
				if($timeConditionSet) {
					$condition .= ' and ';
				}
				$condition .= ' timestamp_ms <= '.$maxTime;
				$timeConditionSet = true;	
			}
		}
		
		// last location is the first result
		$query = 'SELECT * FROM '.$wpdb->prefix.self::LATITUDE_TABLE.' ';
		if($timeConditionSet) {
			$query .= $condition;
		}
		$query .= ' ORDER BY timestamp_ms DESC';
		
		$latitudeHistory = $wpdb->get_results($query);
		
		return $latitudeHistory;
	}
	

	function insert($location) {
		global $wpdb;
		
		// get location name by reverse code
		$locationName = $this->getLocationName($location->getLatitude(), $location->getLongitude());
			
		$result = $wpdb->insert( $wpdb->prefix.self::LATITUDE_TABLE, array(
			'timestamp_ms' => $location->getTimestampMs(), 
			'date' => date("F j, Y, g:i a e", $location->getTimestampMs()/1000), // format date to "July 7, 2011, 12:58 pm UTC"
			'latitude' => $location->getLatitude(),
			'longitude' => $location->getLongitude(),
			'accuracy' => $location->getAccuracy(),
			'location_name' => $locationName
		), array( '%s', '%s', '%s', '%s', '%d', '%s' ) );
		
		return $result;
	}
	
	function update($location) {
		global $wpdb;
		
		// get location name by reverse code
		$locationName = $this->getLocationName($location->getLatitude(), $location->getLongitude());
		
		$result = $wpdb->update( $wpdb->prefix.self::LATITUDE_TABLE, 
			array(
				'timestamp_ms' => $location->getTimestampMs(), 
				'date' => date("F j, Y, g:i a e", $location->getTimestampMs()/1000), // format date to "July 7, 2011, 12:58 pm UTC"
				'latitude' => $location->getLatitude(),
				'longitude' => $location->getLongitude(),
				'accuracy' => $location->getAccuracy(),
				'location_name' => $locationName
			), 
			array( 'timestamp_ms' => $location->getTimestampMs() ), 
			array( '%s', '%s', '%s', '%s', '%d', '%s' ),
			array( '%s') 
		);
		
		return $result;
	}
	
	function delete($timestamps) {
		global $wpdb;
		
		$result = $wpdb->query('DELETE FROM '.$wpdb->prefix.self::LATITUDE_TABLE.' WHERE timestamp_ms in ('.join(',', $timestamps). ');');
		
		return $result;
	}
	
	/**
	* Takes location coordinates and uses Google reverse geocoding service
	* to get the location name
	*
	* @param string latitude
	* @param string longitude
	* @return string location name
	*/
	function getLocationName($lat, $lng) {
		
		// reverse geocode the coordinates to get the location name
		$response = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?sensor=false&latlng='.$lat.','.$lng));
		
		// go through the results to find the formatted address (there are several options)
		$address = array();
		if($response->status == 'OK') {
			foreach($response->results as $result) {
				foreach($result->types as $type) {
					if($type == 'locality') {
						$address['locality'] = $result->formatted_address;
						break;
					} elseif($type == 'administrative_area_level_2') {
						$address['administrative_area_level_2'] = $result->formatted_address;
						break;
					} elseif($type == 'administrative_area_level_1') {
						$address['administrative_area_level_1'] = $result->formatted_address;
						break;
					} elseif($type == 'postal_code') {
						$address['postal_code'] = $result->formatted_address;
						break;
					}
				}
				// most preferred formatted address so let's stop
				if(isset($address['locality'])) {
					break;
				}
			}
		}
		
		// Prefer for formatted address = locality > administrative_area_level_2 > administrative_area_level_1 > postal_code
		$locationName = "";
		if(isset($address['locality'])) {
			$locationName = $address['locality'];
		} elseif(isset($address['administrative_area_level_2'])) {
			$locationName = $address['administrative_area_level_2'];
		} elseif(isset($address['administrative_area_level_1'])) {
			$locationName = $address['administrative_area_level_1'];
		} elseif(isset($address['postal_code'])) {
			$locationName = $address['postal_code'];
		}
	
		return $locationName;
	}


	function createLatitudeTable() {
		global $wpdb;
		
		$this->dropLatitudeTable();
		
   		if($wpdb->get_var("show tables like '".$wpdb->prefix.self::LATITUDE_TABLE."'") != $wpdb->prefix.self::LATITUDE_TABLE) {

   			$sql = "CREATE TABLE " . $wpdb->prefix.self::LATITUDE_TABLE . " (
   				id INT AUTO_INCREMENT PRIMARY KEY,
   				timestamp_ms BIGINT(20) NOT NULL,
   				date VARCHAR(200) NOT NULL,
   				latitude decimal(12,9) NOT NULL,
   				longitude decimal(12,9) NOT NULL,
   				accuracy INT(11) NOT NULL,
   				location_name VARCHAR(512) default NULL,
   				INDEX idx_timestamp_ms (timestamp_ms));";
   			
   			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);
   		}
	}
	
	function dropLatitudeTable() {
		global $wpdb;
		
		$sql = "DROP TABLE IF EXISTS ". $wpdb->prefix.self::LATITUDE_TABLE;
		
		$wpdb->query($sql);
	}
	
	function createApiCacheTable() {
		global $wpdb;
   		
   		// create table for google api php client to store cache
		$sql = "DROP TABLE IF EXISTS ". $wpdb->prefix.self::CACHE_TABLE;
		$wpdb->query($sql);
		
   		if($wpdb->get_var("show tables like '".$wpdb->prefix.self::CACHE_TABLE."'") != $wpdb->prefix.self::CACHE_TABLE) {

   			$sql = "CREATE TABLE " . $wpdb->prefix.self::CACHE_TABLE . " (
 				id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 				k VARCHAR(32),  
 				data TEXT, 
 				last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   				INDEX idx_key (k));";
   			
   			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);
   		}
	}
	
	function dropApiCacheTable() {
		global $wpdb;
		
		$sql = "DROP TABLE IF EXISTS ". $wpdb->prefix.self::CACHE_TABLE;
		
		$wpdb->query($sql);
	}
	
}

?>